import random


class LotteryGame:
    def __init__(self, number_of_ball):
        self.ball_container = list(range(1, number_of_ball))

    def pick_ball_of(self, number_of_ball):
        picked_ball = []
        for _ in range(number_of_ball):
            picked_ball.append(random.choice(self.ball_container))
        picked_ball.sort()
        return picked_ball
