import unittest

from lottery import LotteryGame


class TestLotteryGame(unittest.TestCase):
    def test_pick_ball_of(self):
        number_of_ball = 50
        lottery_game = LotteryGame(number_of_ball)
        random_balls = lottery_game.pick_ball_of(10)
        self.assertEqual(10, len(random_balls), "number of ball in list should equal to number of ball to random")
        self.assertTrue(is_sorted(random_balls), "random_balls list should has ascending order")
        self.assertTrue(random_balls[-1] <= number_of_ball,
                        "max value in list should not greater than biggest number of ball on container")
        self.assertTrue(random_balls[0] >= 1, "min value in list should less than 1")


def is_sorted(lst, key=lambda x: x):
    for i, el in enumerate(lst[1:]):
        if key(el) < key(lst[i]):
            return False
    return True


if __name__ == '__main__':
    unittest.main()
